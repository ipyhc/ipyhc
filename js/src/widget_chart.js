import * as widgets from '@jupyter-widgets/base';
import { extend } from 'lodash';

import * as builder from './widget_builder';

// Import Styles
// import 'highcharts/css/themes/dark-unica.css';
// import 'highcharts/css/themes/grid-light.css';
// import 'highcharts/css/themes/sand-signika.css';

import * as Utils from './widget_utils';
import { version } from '../package.json';

const semver_range = `~${version}`;

const ChartModel = widgets.DOMWidgetModel.extend(
	{
		defaults() {
			return extend(ChartModel.__super__.defaults.call(this), {
				_model_name: 'ChartModel',
				_view_name: 'ChartView',
				_model_module: 'ipyhc',
				_view_module: 'ipyhc',
				_model_module_version: semver_range,
				_view_module_version: semver_range,

				_id: 0,

				theme: '',
				stock: false,
				width: '',
				height: '',

				_options_down: '',
				_data_down: '',
				_data_up: '',
			});
		},
	},
	{
		serializers: _.extend(
			{
				_options_down: { deserialize: Utils.deserialize_params },
				_data_down: { deserialize: Utils.deserialize_params },
				_data_up: { serialize: Utils.serialize_data },
			},
			widgets.DOMWidgetModel.serializers
		),
	}
);

const ChartView = widgets.DOMWidgetView.extend({
	render() {
		const view = this;
		const container = document.createElement('div');

		// Set width and height
		container.style = `width:${view.model.get(
			'width'
		)}; height:${view.model.get('height')}`;
		this.el.appendChild(container);

		const myChart = builder.buildChart(
			view,
			container,
			view.model.get('_options_down'),
			view.model.get('_data_down')
		);
	}
});

export { ChartModel, ChartView };
