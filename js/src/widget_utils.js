import { gzip } from './widget_compress';

const clean_data = (series) => {
    const res = []
    series.forEach(serie => {
        res.push(clean_serie(serie))
    });
    return res;
}

const clean_serie = (serie) => {
    const res = []
    serie.data.forEach(point => {
        res.push({ x: point.x, y: point.y })
    });
    return res;
}

function deserialize_params(value) {
    window.toto = value;
    try {
        return gzip.uncompressBase64ToStr(value, 9);
    } catch (e) {
        console.log('Cannot uncompress');
        console.log(e);
        return value;
    }
    // if (value.constructor === String) {
    //     return value;
    // }
    // return gzip.uncompressBase64ToStr(value, 9);
}

/**
 * Serializes the data_up back to python
 * @param {JSON} value
 */
function serialize_data(value) {
    const compress = gzip.compress(JSON.stringify(value), 9);
    return compress;
}

export {
    clean_data,
    deserialize_params,
    serialize_data,
};