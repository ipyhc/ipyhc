import Highcharts from 'highcharts/highstock';
import Exporting from 'highcharts/modules/exporting';
import Drilldown from 'highcharts/modules/drilldown';
import HighchartsMore from 'highcharts/highcharts-more';
import OfflineExporting from 'highcharts/modules/offline-exporting';
import HeatMap from 'highcharts/modules/heatmap';
import Treemap from 'highcharts/modules/treemap';
import Sunburst from 'highcharts/modules/sunburst';
import { JSONfunc } from './widget_json';
import { clean_data } from './widget_utils';
Exporting(Highcharts);
Drilldown(Highcharts);
HighchartsMore(Highcharts);
OfflineExporting(Highcharts);
HeatMap(Highcharts);
Treemap(Highcharts);
Sunburst(Highcharts);

/**
 * Builds the chart according to the options,
 * in the given div.
 * @param {WidgetView} view
 * @param {HTMLDivElement} div
 * @param {Object} options
 * @param {StringElement} data_json
 */
const buildChart = (view, div, options_json, data_json) => {

    const helpers = {};
    const options = JSONfunc.parse(options_json, helpers);
    const data = JSONfunc.parse(data_json, helpers);
    options.series = data;
    const action = () => {

        const _data_up = { series: clean_data(chart.series) };
        _data_up.xAxis = { min: chart.xAxis[0].min, max: chart.xAxis[0].max }
        _data_up.yAxis = { min: chart.yAxis[0].min, max: chart.yAxis[0].max }
        view.model.set('_data_up', JSONfunc.stringify(_data_up));
        view.touch();
    }

    // Concatenate the user's callback on load.
    const onload = () => {
    };

    // if(typeof options.chart.events.load !== "undefined"){
    //     onload = options.charts.events.load;
    // }
    // console.log(onload)

    options.chart.events = {
        'redraw': function (event) {
            action();
        },
        'load': function (event) {
            onload();

            const this_reflow = () => {this.reflow()};

            setTimeout(function () {
                this_reflow();
            }, 0);
        }
    }

    var chart = {};
    if (!view.model.get('stock')) {
        chart = Highcharts.chart(div, options);
    }
    else {
        chart = Highcharts.stockChart(div, options);
    }

    const export_button = document.createElement('button');
    //div.parentNode.appendChild(export_button);
    export_button.appendChild(document.createTextNode('Export data'));
    export_button.addEventListener('click', action);

    return chart
}

export { buildChart }