# ipyhc

Plugin to use Highcharts in Jupyter notebooks.

### Documentation

Documentation is available on the website [https://ipyhc.gitlab.io/](https://ipyhc.gitlab.io/).

### Credits

This plugin is the result of a collaboration between
[Louis Raison](https://gitlab.com/DGothrek),
[Pierre Tholoniat](https://gitlab.com/tholoz) and [oscar6echo](https://gitlab.com/oscar6echo).