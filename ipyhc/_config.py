import json

SAMPLES_DIR = 'samples'

DF_ONE_IDX_SEVERAL_COL = 'df_one_idx_several_col.csv'
DF_ONE_IDX_SEVERAL_COL_2 = 'df_one_idx_several_col_2.csv'
DF_ONE_IDX_ONE_COL = 'df_one_idx_one_col.csv'
DF_ONE_IDX_TWO_COL = 'df_one_idx_two_col.csv'
DF_TWO_IDX_ONE_COL = 'df_two_idx_one_col.csv'
DF_SCATTER = 'df_scatter.csv'
DF_BUBBLE = 'df_bubble.csv'
DF_HEATMAP = 'df_heatmap.csv'
DF_SEVERAL_IDX_ONE_COL = 'df_several_idx_one_col.csv'
DF_SEVERAL_IDX_ONE_COL_2 = 'df_several_idx_one_col_2.csv'
DF_TWO_IDX_SEVERAL_COL = 'df_two_idx_several_col.csv'
